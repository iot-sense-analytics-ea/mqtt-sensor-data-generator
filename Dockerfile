FROM openjdk:17-alpine
COPY target/mqtt-sensor-data-generator-2.0.1.jar mqtt-sensor-data-generator-2.0.1.jar
COPY src/main/resources/application.properties application.properties
ENTRYPOINT ["java","-jar","/mqtt-sensor-data-generator-2.0.1.jar"]