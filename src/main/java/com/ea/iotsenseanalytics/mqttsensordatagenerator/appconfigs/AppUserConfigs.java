package com.ea.iotsenseanalytics.mqttsensordatagenerator.appconfigs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Provides access to the user configured values set in the application.properties file
 *
 * @author eranga.atugoda@gmail.com
 * Date: APR-2022
 */
@Configuration
public class AppUserConfigs {

    @Value("${mqtt.broker.hivemq.username}")
    public String hiveMqBrokerUserName;

    @Value("${mqtt.broker.hivemq.password}")
    public String hiveMqBrokerPassword;

    @Value("${mqtt.broker.hivemq.url}")
    public String hiveMqBrokerUrl;

    @Value("${mqtt.message.qos.level}")
    public int hiveMqMsgQosLevel;

    @Value("${mqtt.message.retain}")
    public boolean hiveMqMsgRetain;

}
