package com.ea.iotsenseanalytics.mqttsensordatagenerator.services.sensorsignalgenerator;

import com.ea.iotsenseanalytics.mqttsensordatagenerator.appconfigs.AppUserConfigs;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.entities.SensorData;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.entities.ThermostatSensorData;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.utils.GeneralOps;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The implementation class for Thermostat sensor data generation
 * @author eranga.atugoda@gmail.com
 * Date: APR-2022
 */
@Service
public class ThermostatSignalGenerator implements SensorSignalGenerator {
    private static int SENSOR_COUNT = DEFAULT_SENSOR_COUNT;
    private static final String DUMMY_SENSOR_ID_PREFIX = "TS7722_";
    private static final String DEFAULT_SENSOR_CLUSTER_ID = "C0001";
    private static final String DEFAULT_SENSOR_LOCATION = "CMB-LK";
    private static final String DEFAULT_SENSOR_MEASUREMENT_UNIT = "C";

    MqttConnectOptions mqttConnOptions = new MqttConnectOptions();

    /**
     * Increments the no of sensors available by the given number
     *
     * @param sensorCount - No of new sensors of the current type to be added
     */
    public static void incrementSensorCount(int sensorCount) {
        SENSOR_COUNT += sensorCount;
    }

    @Override
    public List<SensorData> generateSignal() {
        List<SensorData> retSensorDataList = new ArrayList<>();

        // Defines a random number generator
        Random random = new Random();

        // Iterates and generate readings for the number of sensors added
        for (int sCount = 1; sCount <= SENSOR_COUNT; sCount++) {
            // Creates an ThermostatSensorData instance to store a new sensor readings
            ThermostatSensorData tSensorData = new ThermostatSensorData();

            // Generates the sensorId by concatenating the prefix with a generated id string padded to given length
            //tSensorData.setSensorId(UUID.randomUUID().toString());
            tSensorData.setId(DUMMY_SENSOR_ID_PREFIX + new GeneralOps().leftPadStr(String.valueOf(sCount), DEFAULT_ID_SUBSTR_LENGTH, DEFAULT_LPAD_CHAR));
            tSensorData.setClustId(DEFAULT_SENSOR_CLUSTER_ID);
            tSensorData.setLoc(DEFAULT_SENSOR_LOCATION);

            //Generates the random temperature within a given range
            float temp = 27 + random.nextInt(3)  + random.nextFloat();
            tSensorData.setTemp(Float.parseFloat(String.format("%.2f", temp)));
            tSensorData.setUnit(DEFAULT_SENSOR_MEASUREMENT_UNIT);

            //Timestamp for the current reading
            tSensorData.setReadTime(new Timestamp(System.currentTimeMillis()));

            retSensorDataList.add(tSensorData);
        }
        return retSensorDataList;
    }

    @Override
    public MqttConnectOptions getMqttConnectionOptions(AppUserConfigs auc) {
        mqttConnOptions.setUserName(auc.hiveMqBrokerUserName);
        mqttConnOptions.setPassword(auc.hiveMqBrokerPassword.toCharArray());
        mqttConnOptions.setAutomaticReconnect(MQTT_OPTS_AUTOMATIC_RECONNECT);
        mqttConnOptions.setConnectionTimeout(MQTT_OPTS_CONNECTION_TIMEOUT);
        mqttConnOptions.setCleanSession(MQTT_OPTS_CLEAN_SESSION);

        return mqttConnOptions;
    }

    @Override
    public String generateSensorTopicGroup() {
        return DEFAULT_SENSOR_GROUP + "/" + ThermostatSensorData.SENSOR_CATEGORY;
    }

    @Override
    public void addNewSensor(int sensorCount) {
        incrementSensorCount(sensorCount);
    }

}