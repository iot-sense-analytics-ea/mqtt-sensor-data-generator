package com.ea.iotsenseanalytics.mqttsensordatagenerator.services.sensorsignalgenerator;

import com.ea.iotsenseanalytics.mqttsensordatagenerator.appconfigs.AppUserConfigs;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.entities.SensorData;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;

import java.util.List;

/**
 * An Interface class that defines the functionalities for sensor data generation
 * @author eranga.atugoda@gmail.com
 * Date: APR-2022
 */
public interface SensorSignalGenerator {
    int DEFAULT_SENSOR_COUNT = 1;
    char DEFAULT_LPAD_CHAR = '0';
    String DEFAULT_SENSOR_GROUP = "iot/sensors";
    int DEFAULT_ID_SUBSTR_LENGTH = 6;

    boolean  MQTT_OPTS_AUTOMATIC_RECONNECT = true;
    boolean MQTT_OPTS_CLEAN_SESSION = true;
    int MQTT_OPTS_CONNECTION_TIMEOUT = 10;

    /**
     * Generates signals for all sensors added for the particular sensor type.
     *  By default there'll be 1 sensor(s) added at the start
     * @return - Generated SensorData object for the particular sensor type
     */
    List<SensorData> generateSignal();

    /**
     * Creates an object with the connection options to be used for the Mqtt connection with the broker
     * @param auc - AppUserConfigs object which contains config values from the Application.properties file
     * @return - MqttConnectOptions instance
     */
    MqttConnectOptions getMqttConnectionOptions(AppUserConfigs auc);

    /**
     * Generates a topic/group for the sensor data to be sent to
     * @return - Generated topic string
     */
    String generateSensorTopicGroup();

    /**
     * Provides ability to add new sensors of the particular sensor category
     * @param sensorCount - No of sensors to add
     */
    void addNewSensor(int sensorCount);
}