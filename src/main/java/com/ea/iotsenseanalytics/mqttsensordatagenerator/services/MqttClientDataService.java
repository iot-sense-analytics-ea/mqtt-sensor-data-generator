package com.ea.iotsenseanalytics.mqttsensordatagenerator.services;

import com.ea.iotsenseanalytics.mqttsensordatagenerator.appconfigs.AppUserConfigs;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.entities.SensorData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Provides functionalities needed for connecting with the MQTT Broker and to submit the sensor readings
 * @author eranga.atugoda@gmail.com
 * Data: APR-2022
 */
public class MqttClientDataService {
    static List<Map<String,MqttClient>> MQTT_CLIENT_CONNECTIONS_LIST = new ArrayList<>();
    ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Iteratively submits the sensor reading messages to the MQTT Broker
     * @param connOpts - MQTT Connection options to use
     * @param sensorsDataList - Sensor readings generated from 1 or more sensor(s) of the same category
     * @param sensorTopicName - Topic name to submit the data at the MQTT Broker
     * @param appUserConfigs - To get the user configured values in the Applications.properties file
     */
    public void submitSensorDataToBroker(MqttConnectOptions connOpts, List<SensorData> sensorsDataList, String sensorTopicName,
                                         AppUserConfigs appUserConfigs) throws Exception{

        for(SensorData sensorData : sensorsDataList){
            //Gets the MQTT client connection
            MqttClient mqttClient = getConnection(sensorData.getId(), appUserConfigs.hiveMqBrokerUrl, connOpts);

            //Converts the sensor data to string
            String payload = objectMapper.writeValueAsString(sensorData);

            //Creates an MQTT Message type using the sensor data
            MqttMessage mqttMsg = new MqttMessage(payload.getBytes());
            mqttMsg.setQos(appUserConfigs.hiveMqMsgQosLevel);
            mqttMsg.setRetained(appUserConfigs.hiveMqMsgRetain);

            //Publishes the message to the given topic at the MQTT Broker
            mqttClient.publish(sensorTopicName, mqttMsg);
        }
    }



    /**
     * Function to make sure only one connection is made per clientId to the MQTT Broker
     * Creates new MQTT Connection if not exists, otherwise returns the existing one for the clientId
     * @param clientId - Client ID / Sensor ID for the sensor
     * @param mqttBrokerURI - URI to connect to the MQTT Broker
     * @param options - MQTT Option to use
     * @return - Returns a MQTT Client
     */
    public MqttClient getConnection(String clientId, String mqttBrokerURI, MqttConnectOptions options) throws Exception{
        MqttClient retClient;

        List<Map<String,MqttClient>> existingConns = MQTT_CLIENT_CONNECTIONS_LIST.stream()
                .filter(x -> x.containsKey(clientId))
                .toList();

        //If the clientId does not exists in the List then creates and adds a new MQTT client
        if(existingConns.isEmpty()){
            Map<String,MqttClient> newConnMap = new HashMap<>();
            retClient = setMqttClient(mqttBrokerURI,clientId,options);
            newConnMap.put(clientId, retClient);
            MQTT_CLIENT_CONNECTIONS_LIST.add(newConnMap);
        } else {
            // If the clientId exists then returns the MQTT client object
            retClient = existingConns.get(0).get(clientId);
        }
        return retClient;
    }


    /**
     * Cretes a MQTT connection for the clientId and other options provided
     * @param mqttBrokerURI  - URI to connect to the MQTT Broker
     * @param clientId - Client ID / Sensor ID for the sensor
     * @param options - MQTT Option to use
     * @return - Returns a MQTT Client connection
     */
    private MqttClient setMqttClient(String mqttBrokerURI, String clientId, MqttConnectOptions options) throws Exception{
        MqttClient mqttClient = new MqttClient(mqttBrokerURI, clientId, null);
        mqttClient.connect(options);
        return mqttClient;
    }

}
