package com.ea.iotsenseanalytics.mqttsensordatagenerator.services.sensorsignalgenerator;

import com.ea.iotsenseanalytics.mqttsensordatagenerator.appconfigs.AppUserConfigs;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.entities.HeartRateSensorData;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.entities.SensorData;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.utils.GeneralOps;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * The implementation class for Heart Rate sensor data generation
 * @author eranga.atugoda@gmail.com
 * Date: APR-2022
 */
@Service
public class HeartRateSignalGenerator implements SensorSignalGenerator {
    private static int SENSOR_COUNT = DEFAULT_SENSOR_COUNT;
    private static final String DUMMY_SENSOR_ID_PREFIX = "HS-RTB_";
    private static final String DEFAULT_SENSOR_MEASUREMENT_UNIT = "BPM";
    private static final String DEFAULT_SENSOR_BODY_LOCATION = "WRIST";
    private static final float BATTERY_LEVEL_START_VALUE = 80;
    private static float CUMULATIVE_BATTERY_LEVEL_DROP = 1/100;

    MqttConnectOptions mqttConnOptions = new MqttConnectOptions();

    /**
     * Increments the no of sensors available by the given number
     * @param sensorCount - No of new sensors of the current type to be added
     */
    public static void incrementSensorCount(int sensorCount) {
        SENSOR_COUNT += sensorCount;
    }


    @Override
    public List<SensorData> generateSignal() {

        List<SensorData> retSensorDataList = new ArrayList<>();

        // Defines a random number generator
        Random random = new Random();

        // Iterates and generate readings for the number of sensors added
        for(int sCount=1; sCount<=SENSOR_COUNT; sCount++) {
            // Creates an ThermostatSensorData instance to store a new sensor readings
            HeartRateSensorData hSensorData = new HeartRateSensorData();

            // Generates the sensorId by concatenating the prefix with a generated id string padded to given length
            hSensorData.setId(DUMMY_SENSOR_ID_PREFIX + new GeneralOps().leftPadStr(String.valueOf(sCount), DEFAULT_ID_SUBSTR_LENGTH, DEFAULT_LPAD_CHAR));
            hSensorData.setUnit(DEFAULT_SENSOR_MEASUREMENT_UNIT);
            hSensorData.setLoc(DEFAULT_SENSOR_BODY_LOCATION);

            //Generates random heart rate level
            hSensorData.setHeartRate(random.nextInt(25) + 50);

            //Timestamp for the current reading
            hSensorData.setReadTime(new Timestamp(System.currentTimeMillis()));

            //Generates a slowly dropping cumulative battery percentage value
            CUMULATIVE_BATTERY_LEVEL_DROP += random.nextFloat()/10;
            //Re-charge battery if it drains more than 60%
            CUMULATIVE_BATTERY_LEVEL_DROP = (CUMULATIVE_BATTERY_LEVEL_DROP > 60) ? 1/100 : CUMULATIVE_BATTERY_LEVEL_DROP;
            hSensorData.setBatteryPct(Float.parseFloat( String.format("%.2f", (BATTERY_LEVEL_START_VALUE-CUMULATIVE_BATTERY_LEVEL_DROP)) ));

            retSensorDataList.add(hSensorData);
        }
        return retSensorDataList;
    }



    @Override
    public MqttConnectOptions getMqttConnectionOptions(AppUserConfigs auc) {
        mqttConnOptions.setUserName(auc.hiveMqBrokerUserName);
        mqttConnOptions.setPassword(auc.hiveMqBrokerPassword.toCharArray());
        mqttConnOptions.setAutomaticReconnect(MQTT_OPTS_AUTOMATIC_RECONNECT);
        mqttConnOptions.setConnectionTimeout(MQTT_OPTS_CONNECTION_TIMEOUT);
        mqttConnOptions.setCleanSession(MQTT_OPTS_CLEAN_SESSION);

        return mqttConnOptions;
    }

    @Override
    public String generateSensorTopicGroup() {
        return DEFAULT_SENSOR_GROUP + "/" + HeartRateSensorData.SENSOR_CATEGORY;
    }

    @Override
    public void addNewSensor(int sensorCount) {
        incrementSensorCount(sensorCount);
    }
}
