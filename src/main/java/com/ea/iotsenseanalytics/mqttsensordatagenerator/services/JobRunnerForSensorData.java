package com.ea.iotsenseanalytics.mqttsensordatagenerator.services;

import com.ea.iotsenseanalytics.mqttsensordatagenerator.appconfigs.AppUserConfigs;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.controllers.SensorSimulatorRESTController;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.services.sensorsignalgenerator.FuelLevelSignalGenerator;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.services.sensorsignalgenerator.HeartRateSignalGenerator;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.services.sensorsignalgenerator.ThermostatSignalGenerator;


/**
 * A Job executor that would invoke the generation and the submission of the sensor data for different sensor categories
 * @author eranga.atugoda@gmail.com
 * Date: APR-2022
 */
public class JobRunnerForSensorData implements Runnable {
    ThermostatSignalGenerator thermostatSigGen;
    FuelLevelSignalGenerator fuelSigGen;
    HeartRateSignalGenerator heartRateSigGen;
    AppUserConfigs appUserConfigs;

    MqttClientDataService mqttSensorDataSubmitService = new MqttClientDataService();

    /**
     * Injects the classes from the bean store to re-use the instances to preserve the configs and minimize memory usage
     */
    public JobRunnerForSensorData(ThermostatSignalGenerator tsg,
                                  FuelLevelSignalGenerator fsg,
                                  HeartRateSignalGenerator hsg,
                                  AppUserConfigs auc) {
        thermostatSigGen = tsg;
        fuelSigGen = fsg;
        heartRateSigGen = hsg;
        appUserConfigs = auc;
    }


    @Override
    public void run() {

        while(SensorSimulatorRESTController.SENSOR_DATA_GEN_STARTED) {
            try {
                //Generates and submits Fuel-level sensor readings
                mqttSensorDataSubmitService.submitSensorDataToBroker(
                        fuelSigGen.getMqttConnectionOptions(appUserConfigs),
                        fuelSigGen.generateSignal(),
                        fuelSigGen.generateSensorTopicGroup(),
                        appUserConfigs);

                //Generates and submits heart-rate sensor readings
                mqttSensorDataSubmitService.submitSensorDataToBroker(
                        heartRateSigGen.getMqttConnectionOptions(appUserConfigs),
                        heartRateSigGen.generateSignal(),
                        heartRateSigGen.generateSensorTopicGroup(),
                        appUserConfigs);

                //Generates and submits thermostat sensor readings
                mqttSensorDataSubmitService.submitSensorDataToBroker(
                        thermostatSigGen.getMqttConnectionOptions(appUserConfigs),
                        thermostatSigGen.generateSignal(),
                        thermostatSigGen.generateSensorTopicGroup(),
                        appUserConfigs);

                //Sleep for 1 second
                Thread.sleep(1000);
            } catch (Exception e){
                System.out.println(e.getMessage());
            }

        }
    }
}
