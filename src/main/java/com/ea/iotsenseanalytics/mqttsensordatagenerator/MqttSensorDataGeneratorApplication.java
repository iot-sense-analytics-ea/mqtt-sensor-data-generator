package com.ea.iotsenseanalytics.mqttsensordatagenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MqttSensorDataGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MqttSensorDataGeneratorApplication.class, args);
	}

}
