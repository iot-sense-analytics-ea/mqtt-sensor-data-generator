package com.ea.iotsenseanalytics.mqttsensordatagenerator.utils;

import org.springframework.stereotype.Component;

/**
 * A Class with functionalities required for general operations and calculations
 *  @author eranga.atugoda@gmail.com
 *  Date: APR-2022
 */
@Component
public class GeneralOps {

    /**
     * Generates a string of given length, where it is left padded with the given character
     * @param inputString - Input String
     * @param reqLength - Required length of the final string
     * @param padCharacter - Character to be used for padding
     * @return - String with required length
     */
    public String leftPadStr(String inputString, int reqLength, char padCharacter) {
        if (inputString.length() >= reqLength) {
            return inputString;
        }
        StringBuilder sb = new StringBuilder();
        while (sb.length() < (reqLength - inputString.length()) ) {
            sb.append(padCharacter);
        }
        sb.append(inputString);

        return sb.toString();
    }
}
