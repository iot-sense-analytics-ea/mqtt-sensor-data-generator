package com.ea.iotsenseanalytics.mqttsensordatagenerator.entities;

/**
 * Class for FUEL_LEVEL sensor data which extends the Abstract class SensorData
 *  fuelLevelReading - Fuel level reading of the sensor
 *
 * @author eranga.atugoda@gmail.com
 * Date: APR-2022
 */
public class FuelLevelSensorData extends SensorData{
    public final static String SENSOR_CATEGORY = SENSOR_CATEGORY_FUEL_LEVEL;
    private float fuelLevel;

    //Getters for the class variables
    public float getFuelLevel() {
        return fuelLevel;
    }
    public String getCategory() { return SENSOR_CATEGORY; }

    //Setters for the class variables
    public void setFuelLevel(float fuelLevel) {
        this.fuelLevel = fuelLevel;
    }
}
