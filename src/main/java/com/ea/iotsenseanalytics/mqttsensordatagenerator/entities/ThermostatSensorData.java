package com.ea.iotsenseanalytics.mqttsensordatagenerator.entities;

/**
 * Class for THERMAL sensor data which extends the Abstract class SensorData
 *  temperature - Temperature reading of the sensor
 *  sensorLocation -  A user pre-defined location where the sensor is installed
 *  sensorClusterId - Id of the cluster the temperature sensor belongs to when there are multiple sensors are installed
 *                      for a given location
 *
 * @author eranga.atugoda@gmail.com
 * Date: APR-2022
 */
public class ThermostatSensorData extends SensorData {
    public final static String SENSOR_CATEGORY = SENSOR_CATEGORY_THERMOSTAT;
    private float temp;
    private String loc;
    private String clustId;

    //Getters for the class variables
    public float getTemp() {
        return temp;
    }
    public String getLoc() {
        return loc;
    }
    public String getClustId() {
        return clustId;
    }
    public String getCategory() { return SENSOR_CATEGORY; }

    //Setters for the class variables
    public void setTemp(float temp) {
        this.temp = temp;
    }
    public void setLoc(String loc) {
        this.loc = loc;
    }
    public void setClustId(String clustId) {
        this.clustId = clustId;
    }
}

