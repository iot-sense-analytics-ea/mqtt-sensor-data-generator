package com.ea.iotsenseanalytics.mqttsensordatagenerator.entities;

/**
 * Class for THERMAL sensor data which extends the Abstract class SensorData
 *  heartRateReading - Heart rate reading of the sensor
 *  sensorBodyLocation -  A user defined location where the sensor is installed
 *  batteryPercentage - Remaining battery level percentage of the sensor
 *
 * @author eranga.atugoda@gmail.com
 * Date: APR-2022
 */
public class HeartRateSensorData extends SensorData{
    public final static String SENSOR_CATEGORY = SENSOR_CATEGORY_HEART_RATE;
    private int heartRate;
    private String loc;
    private float batteryPct;

    //Getters for the class variables
    public int getHeartRate() {
        return heartRate;
    }
    public String getLoc() {
        return loc;
    }
    public float getBatteryPct() {
        return batteryPct;
    }
    public String getCategory() { return SENSOR_CATEGORY; }

    //Setters for the class variables
    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }
    public void setLoc(String loc) {
        this.loc = loc;
    }
    public void setBatteryPct(float batteryPct) {
        this.batteryPct = batteryPct;
    }
}
