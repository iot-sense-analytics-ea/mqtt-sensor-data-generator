package com.ea.iotsenseanalytics.mqttsensordatagenerator.entities;

import java.sql.Timestamp;

/**
 * An Abstract class that defines the template for all sensor data objects
 * sensorId - Unique id of the sensor
 * sensorCategory -  Category of the sensor Eg: Thermal, HeartRate, FuelLevel etc
 * sensorTime - The time the sensor data was captured
 * sensorGroupId - The id of the group the sensor belongs to of a user-defined heterogeneous group of sensors
 *                  for eg: Thermal, Fuel level sensors that belongs to the same subject
 *
 * @author eranga.atugoda@gmail.com
 * Date: APR-2022
 */
public abstract class SensorData {

    // Defines the constants for sensor category types
    public static final String SENSOR_CATEGORY_THERMOSTAT = "thermostat";
    public static final String SENSOR_CATEGORY_HEART_RATE = "heart-rate";
    public static final String SENSOR_CATEGORY_FUEL_LEVEL = "fuel-level";
    public static final String SENSOR_CATEGORY_GENERIC = "generic";
    private String id;
    private Timestamp readTime;
    private String unit;

    // Getters for the class variables
    public String getId() {
        return id;
    }
    public Timestamp getReadTime() {
        return readTime;
    }
    public String getUnit() { return unit; }

    // Setters for the class variables
    public void setId(String id) { this.id = id; }
    public void setReadTime(Timestamp readTime) {
        this.readTime = readTime;
    }
    public void setUnit(String unit) { this.unit = unit; }

}
