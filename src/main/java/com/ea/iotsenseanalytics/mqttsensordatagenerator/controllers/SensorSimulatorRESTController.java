package com.ea.iotsenseanalytics.mqttsensordatagenerator.controllers;

import com.ea.iotsenseanalytics.mqttsensordatagenerator.appconfigs.AppUserConfigs;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.entities.SensorData;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.services.JobRunnerForSensorData;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.services.sensorsignalgenerator.FuelLevelSignalGenerator;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.services.sensorsignalgenerator.HeartRateSignalGenerator;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.services.sensorsignalgenerator.ThermostatSignalGenerator;
import com.ea.iotsenseanalytics.mqttsensordatagenerator.utils.GeneralOps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@CrossOrigin
@RestController
@RequestMapping("/sensor/simulator")
public class SensorSimulatorRESTController {

    @Autowired
    GeneralOps generalOps;

    @Autowired
    ThermostatSignalGenerator thermostatSigGen;

    @Autowired
    FuelLevelSignalGenerator fuelLevelSigGen;

    @Autowired
    HeartRateSignalGenerator heartRateSigGen;

    @Autowired
    AppUserConfigs appUserConfigs;

    public static boolean SENSOR_DATA_GEN_STARTED = false;
    ExecutorService pool = Executors.newFixedThreadPool(20);
    private static final String BASE_PATH = "/sensor/simulator";


    /**
     * End-point for STARTING or STOPPING the Sensor Data Generation Service
     * @param runStimulator - Use 'true' to Start, 'false' to stop
     *
     * Example URL:
     * http://localhost:<port>/sensor/simulator/stimulate-all-sensors?run=true
     */
    @RequestMapping(method = RequestMethod.GET, path = {"/stimulate-all-sensors"})
    public ResponseEntity<List<Map<String,String>>> stimulateAllSensors(@RequestParam(value = "run") boolean runStimulator) {

        String retMsg = null;

        if(runStimulator){
            if(!SENSOR_DATA_GEN_STARTED){
                try {
                    SENSOR_DATA_GEN_STARTED = true;

                    //Creates a Job Runner and submits the task to a job pool to make the user request Non-Blocking
                    JobRunnerForSensorData sensorDataJobService = new JobRunnerForSensorData(
                            thermostatSigGen, fuelLevelSigGen, heartRateSigGen, appUserConfigs);
                    Runnable worker = sensorDataJobService;
                    pool.execute(worker);

                    retMsg = "Sensor data generation has Started";
                } catch (Exception e){
                    System.out.println(e);
                }
            } else {
                retMsg = "Sensors are already generating data. \n" +
                        "To stop use the URL: http://<host>:<port>" + BASE_PATH + "/stimulate-all-sensors?run=false \n" +
                        "To add more sensors use URL: http://<host>:<port>" + BASE_PATH + "/add-new-sensors?sensor-category=thermostat&sensor-count=2";
            }
        } else {
            SENSOR_DATA_GEN_STARTED = false;
            retMsg = "Sensor data generation will be Stopped";
        }

        List<Map<String,String>> responseList = new ArrayList<>();
        Map<String,String> resMap = new HashMap<>();
        resMap.put("RESPONSE", retMsg);
        responseList.add(resMap);

        ResponseEntity<List<Map<String,String>>> resEntity = new ResponseEntity<>(responseList,HttpStatus.OK);

        return resEntity;
    }



    /**
     * End-point for adding more sensors of the required type along with the number of sensors to add
     * @param sensorCategory - Example values: thermostat, fuellevel, heartrate
     * @param sensorCount - No of sensors to add
     *
     * Example URL:
     * http://localhost:<port>/sensor/simulator/add-new-sensors?sensor-category=thermostat&sensor-count=2
     */
    @RequestMapping(method = RequestMethod.GET, path = {"/add-new-sensors"})
    public ResponseEntity<List<Map<String,String>>> addNewSensors(@RequestParam(value = "sensor-category") String sensorCategory,
                                                                  @RequestParam(value = "sensor-count") int sensorCount) {
        String retMsg = null;

        if(sensorCount > 0) {
            if (sensorCategory.equalsIgnoreCase(SensorData.SENSOR_CATEGORY_THERMOSTAT)) {
                thermostatSigGen.addNewSensor(sensorCount);
                retMsg = sensorCount + " more thermostat sensor(s) added";
            }
            if (sensorCategory.equalsIgnoreCase(SensorData.SENSOR_CATEGORY_FUEL_LEVEL)) {
                fuelLevelSigGen.addNewSensor(sensorCount);
                retMsg = sensorCount + " more fuel level sensor(s) added";
            }
            if (sensorCategory.equalsIgnoreCase(SensorData.SENSOR_CATEGORY_HEART_RATE)) {
                heartRateSigGen.addNewSensor(sensorCount);
                retMsg = sensorCount + " more heart rate sensor(s) added";
            }
        } else {
            retMsg = "Invalid number of sensors specified. Please rectify";
        }

        // If unknown sensor category provided
        retMsg = ((retMsg == null) ? "Invalid Sensor Category. Should be one of ("
                + SensorData.SENSOR_CATEGORY_THERMOSTAT + "," + SensorData.SENSOR_CATEGORY_FUEL_LEVEL + "," +
                SensorData.SENSOR_CATEGORY_HEART_RATE + ")" : retMsg );

        List<Map<String,String>> responseList = new ArrayList<>();
        Map<String,String> resMap = new HashMap<>();
        resMap.put("RESPONSE", retMsg);
        responseList.add(resMap);

        ResponseEntity<List<Map<String,String>>> resEntity = new ResponseEntity<>(responseList,HttpStatus.OK);

        return resEntity;
    }
}
